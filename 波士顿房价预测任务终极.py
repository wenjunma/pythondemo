import numpy as np
import matplotlib.pyplot as plt
def load_data():
    datafile = 'D:\大一下 小学期/boston'
    data = np.fromfile(datafile,sep=' ')
    feature_names = ['CRIM','ZN','INDUS','CHAS','NOX','RM','AGE',\
                     'DIS','RAD','TAX','PTRATIO','B','LSTAT','MEDV']
    feature_num = len(feature_names)
    data = data.reshape([data.shape[0] // feature_num, feature_num])

    ratio = 0.8
    offset = int(data.shape[0] * ratio)
    training_data = data[:offset]

    maximums, minimums, avgs = training_data.max(axis=0),training_data.min(axis=0), \
    training_data.sum(axis=0) / training_data.shape[0]

    for i in range(feature_num):
        data[:,i] = (data[:, i] - avgs[i]) / (maximums[i] - minimums[i])
        training_data = data[: offset]
        test_data = data[offset:]
        return training_data, test_data

    training_data, test_data = load_data()
    x = training_data[:,:-1]
    y = training_data[:,-1:]

    print(x[0])
    print(y[0])
#hello